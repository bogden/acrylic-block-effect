import html2canvas from 'html2canvas';

export default class BlurEffect {
  constructor(element = null) {
    this.element = element;
    this.blur();
  }

  blur() {
    if (this.element) {
      setTimeout(() => {
        html2canvas(document.body).then((canvas) => {
          $(this.element).append(canvas);
          $(canvas).attr('id', 'canvas');
        });

        $(window).scroll(function() {
          $('#canvas').css(
              '-webkit-transform',
              'translatey(-' + $(window).scrollTop() + 'px)');
        });

        window.onresize = function() {
          $('#canvas').width($(window).width());
        };

        $(document).bind('touchmove', function() {
          $('#canvas').css(
              '-webkit-transform',
              'translatey(-' + $(window).scrollTop() + 'px)');
        });

        $(document).bind('touchend', function() {
          $('#canvas').css(
              '-webkit-transform',
              'translatey(-' + $(window).scrollTop() + 'px)');
        });

      }, 2000);
    }
  }
}
